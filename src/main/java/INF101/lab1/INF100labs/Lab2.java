package INF101.lab1.INF100labs;

//import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
//import java.util.Collections;

/**
 * Implement the methods findLongestWords, isLeapYear and isEvenPositiveInt.
 * These programming tasks was part of lab2 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/2/
 */

public class Lab2 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        findLongestWords("cool", "school", "drool");
        isLeapYear(2022);
        isEvenPositiveInt(6);
    }

    // OPPGAVE 1 Find Longest Word
    public static void findLongestWords(String word1, String word2, String word3) {
        // Lager en liste av ordene
        String[] words = new String[]{word1, word2, word3};
        List<String> listFromArray = Arrays.asList(words);

        /* ^^ dette er en litt mer fancy måte å skrive det som står under
        List<String> ordliste = new ArrayList<String>();
        ordliste.add(word1);
        ordliste.add(word2);
        ordliste.add(word3);
        */

        // For loop iterating over the List and find maxLength
        int maxLength = 0;
        for (int i = 0; i < listFromArray.size(); i++) {

            // Saving the length of the words
            int wordLength = listFromArray.get(i).length();
            // Defining maxLength as the longest
            if (wordLength > maxLength) {
                maxLength = wordLength;
            }

        }

        // For loop iterating over the list and printing the words that have the same length as maxLength
        for (int i = 0; i < listFromArray.size(); i++) {

            int wordLength = listFromArray.get(i).length();

            if (wordLength == maxLength) {
                System.out.println(listFromArray.get(i));
            }

        }

    }

    // OPPGAVE 2 Is Leap Year
    public static boolean isLeapYear(int year) {

        /*Vanligvis er et år som er delelig med 4 et skuddår (for eksempel 1996 var et skuddår);
        bortsett fra år som også er delelige med 100 (for eksempel 1900 er ikke skuddår);
        men hvis året som er delelige med 100 også er delelig med 400, da er det et skuddår likevel (for eksempel er 2000 et skuddår).
        */

        if (year % 4 == 0) {
            if (year % 100 == 0) {
                return (year % 400 == 0);
            }
        }
        return (year % 4 == 0);

    }

    // OPPGAVE 3 Is Even Positive Int
    public static boolean isEvenPositiveInt(int num) {

        /* Metoden har et heltall num som parameter. 
        Metoden skal returnere true hvis num er et positivt partall, og false ellers.
         */

        if (num > 0) {
            return (num % 2 == 0);
            }
        return (num > 0);
    }
}