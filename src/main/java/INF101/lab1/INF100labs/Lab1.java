package INF101.lab1.INF100labs;

import java.util.Scanner;

/**
 * Implement the methods task1, and task2.
 * These programming tasks was part of lab1 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/1/
 */
public class Lab1 {

    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        task1();
        task2();
        
    }

    // TASK 1
    public static void task1() {
        System.out.println("Hei, det er meg, datamaskinen.");
        System.out.println("Hyggelig å se deg her.");
        System.out.println("Lykke til med INF101!");
    }

    // TASK 2
    public static void task2() {
        sc = new Scanner(System.in); // Do not remove this line
        String navn;
        String adresse;
        String postnummerOgPoststed;

        System.out.println("Hva er ditt navn?");
        // taking string input
        navn = sc.nextLine();

        System.out.println("Hva er din adresse?");
        // taking string input
        adresse = sc.nextLine();

        System.out.println("Hva er ditt postnummer og poststed?");
        // taking string input
        postnummerOgPoststed = sc.nextLine();

        // print string
        System.out.println(navn + "s adresse er:");
        System.out.println("");
        System.out.println(navn);
        System.out.println(adresse);
        System.out.println(postnummerOgPoststed);

    }

}

    // Eksempel på metode som tar input fra spørsmål eller tekst

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
    
    public static String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.nextLine();
        return userInput;
    }

    */


    //Ctrl * for å gjøre alt du har markert til kommentarer! wuhu