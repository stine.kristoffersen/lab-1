package INF101.lab1.INF100labs;

/**
 * Implement the methods multiplesOfSevenUpTo, multiplicationTable and crossSum.
 * These programming tasks was part of lab3 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/3/
 */
public class Lab3 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        //multiplesOfSevenUpTo(49);
        //multiplicationTable(5);
        //crossSum(1234);
    
    }

    public static void multiplesOfSevenUpTo(int n) {
        int luckyNumber = 7;
        while (n > luckyNumber) {
            System.out.println(luckyNumber);
            luckyNumber += 7;
        }
        System.out.println(n);
    }

    public static void multiplicationTable(int n) {
        for (int i=1; i <= n; i++) {
            String text = i+":";
            for (int j=1; j <= n; j++) {
                text += " " + i*j;
            }
            System.out.println(text);
        }
    }

    public static int crossSum(int n) {
        int sum = 0;

        while (n >= 1) {
            sum += n % 10;
            n = n / 10;
        }
        return sum;
    }
}