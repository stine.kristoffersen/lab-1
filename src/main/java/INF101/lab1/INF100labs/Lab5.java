package INF101.lab1.INF100labs;

import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.List;
import java.util.Arrays;

/**
 * Implement the methods removeThrees, uniqueValues and addList.
 * These programming tasks was part of lab5 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/5/
 */
public class Lab5 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

        ArrayList<Integer> a = new ArrayList<>(Arrays.asList(1, 2, 3));
        ArrayList<Integer> b = new ArrayList<>(Arrays.asList(4, 2, -3));

        addList(a, b);

    }

    public static ArrayList<Integer> multipliedWithTwo(ArrayList<Integer> list) {
        ArrayList<Integer> multiplyList = new ArrayList<>();

        for (int i=0; i < list.size() ; i++) {
            multiplyList.add(2*list.get(i));  // Multipliserer her med 2!
        }
        return multiplyList;
    }

    public static ArrayList<Integer> removeThrees(ArrayList<Integer> list) {
        ArrayList<Integer> removedThreesList = new ArrayList<>();

        for (int i=0; i < list.size(); i++) {
            if (list.get(i) != 3) {
                removedThreesList.add(list.get(i));   // Legger her til i den nye listen kun dersom tallet ikke er 3!
            }
        }
        return removedThreesList;
    }

    public static ArrayList<Integer> uniqueValues(ArrayList<Integer> list) {
        ArrayList<Integer> uniqueList = new ArrayList<>();

        // Har originalt en liste med kun ett element.
        // Når jeg legger til et nytt element sjekker jeg om elementet finnes i listen fra før.
        // Hvis det allerede finnes i listen fra før og jeg nå har to av samme tall i listen,
        // fjernes det tallet som ble lagt til sist.

        for (int i = 0; i < list.size(); i++) {
            int count = 0;
            uniqueList.add(list.get(i));
            System.out.println(uniqueList);
            for (int j = 0; j < uniqueList.size(); j++) {
                if (list.get(i)==uniqueList.get(j)) {
                    count += 1;
                }
                if (count > 1) {
                    uniqueList.remove(j);  // Her fjernes det tallet som ble lagt til sist
                }
            }
        }
        return uniqueList;
    }

    public static void addList(ArrayList<Integer> a, ArrayList<Integer> b) {
        ArrayList<Integer> productList = new ArrayList<>();

        for (int i=0; i < a.size(); i++) {
            productList.add(a.get(i) + b.get(i));
        }

        for (int j=0; j < productList.size(); j++) {
            a.set(j, productList.get(j));
        }
    }
}