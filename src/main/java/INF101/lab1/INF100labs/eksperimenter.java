package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.Arrays;

public class eksperimenter {

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        ArrayList<Integer> list1 = new ArrayList<>(Arrays.asList(1, 2, 3, 3, 2));
        ArrayList<Integer> removedList1 = uniqueValues(list1);
        System.out.println(removedList1); // [1, 2, 3]
    }

    public static ArrayList<Integer> uniqueValues(ArrayList<Integer> list) {
        ArrayList<Integer> uniqueList = new ArrayList<>();

        // Har originalt en liste med kun ett element.
        // Når jeg legger til et nytt element sjekker jeg først om elementet finnes i listen fra før.

        for (int i = 0; i < list.size(); i++) {
            int count = 0;
            uniqueList.add(list.get(i));
            System.out.println(uniqueList);
            for (int j = 0; j < uniqueList.size(); j++) {
                if (list.get(i)==uniqueList.get(j)) {
                    count += 1;
                }
                if (count > 1) {
                    uniqueList.remove(j);
                }
            }
        }
        return uniqueList;
    }
}


