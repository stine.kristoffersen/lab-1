package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Implement the methods removeRow and allRowsAndColsAreEqualSum.
 * These programming tasks was part of lab7 in INF100 fall 2022/2023. You can find
 * them here: https://inf100h22.stromme.me/lab/7/
 */
public class Lab7 {

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

        ArrayList<ArrayList<Integer>> grid2 = new ArrayList<>();
        grid2.add(new ArrayList<>(Arrays.asList(3, 4, 6)));
        grid2.add(new ArrayList<>(Arrays.asList(0, 5, 8)));
        grid2.add(new ArrayList<>(Arrays.asList(9, 3, 1)));

        boolean equalSums2 = allRowsAndColsAreEqualSum(grid2);
        System.out.println(equalSums2); // false


    }

    public static void removeRow(ArrayList<ArrayList<Integer>> grid, int row) {
        grid.remove(row);
    }

    public static boolean allRowsAndColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {
        ArrayList<Integer> sumList = new ArrayList<>();

        // OBS OBS OBS NY MÅTE Å ITERERE OVER LISTE PÅ

        // Retrieving rows and adding the sums to sumList

        for (ArrayList<Integer> i : grid) {
            int sum = 0;
            for (int j : i) {
                sum += j;
            }
            sumList.add(sum);
        }

        // Retrieving the columns and adding the sums to sumList
        for (int num = 0; num < grid.get(0).size(); num++) { // antall columns
            int sum = 0;

            for (ArrayList<Integer> i : grid) { // rows
                sum += i.get(num);
            }

            sumList.add(sum);
        }

        for (int ele = 1; ele+1 < sumList.size(); ele++) {
            if (sumList.get(ele) != sumList.get(ele-1)) {
                return false;
            }
        }
    return true;
    }
}


/*  KAN SKRIVE FOR-LOOP SLIK

for (int element : myList) {
    System.out.println(element)
}

*/